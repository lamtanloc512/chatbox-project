import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useDispatch } from 'react-redux';
import { updateMessagesState, fetchApi } from '../redux/messagesSlice';
import ButtonRecorder from './ButtonRecorder';

const Footer = () => {
  const [content, setContent] = useState({ text: '', isBot: false, url: '' });
  const dispatch = useDispatch();
  // const messages = useSelector((state) => state.messages.messages);
  const voice = useSelector((state) => state.messages.voice);
  const dataToSpeechApi = useSelector(
    (state) => state.messages.dataToSpeechApi
  );
  const [newContent, setNewContent] = useState({
    text: '',
    isBot: true,
    url: '',
  });

  // useEffect(() => {
  //   console.log(voice);
  // }, [voice]);

  // kiểm soát submit form
  const handleSubmit = (e) => {
    e.preventDefault();

    if (content.text !== '') {
      dispatch(updateMessagesState(content));
      dispatch(fetchApi(content));
    } else {
      alert('Vui lòng nhập tin nhắn!');
    }

    setContent(() => {
      return { text: '', isBot: false, url: '' };
    });
  };

  async function fetchSpeechApi(paramTextToSend, paramVoiceToChoose) {
    let myHeaders = new Headers();
    myHeaders.append(
      'token',
      'PfVgD7TvTGFBYGfuUA9bnnWqkClxi7CVSuRgt2aS-juDOpQ88fiMYaa87IlNlRk3'
    );
    myHeaders.append('Content-Type', 'application/json');
    let raw = JSON.stringify({
      text: paramTextToSend,
      voice: paramVoiceToChoose,
      id: '2',
      without_filter: false,
      speed: 1,
      tts_return_option: 2,
    });

    let requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow',
    };

    let vNewMessageObj = {
      isAudio: false,
      isBot: true,
      text: 'Bạn cần chúng tôi hỗ trợ gì?',
      url: '',
    };

    await fetch(
      'https://viettelgroup.ai/voice/api/tts/v1/rest/syn',
      requestOptions
    )
      .then((response) => response.body)
      .then((body) => {
        const reader = body.getReader();
        return new ReadableStream({
          start(controller) {
            return pump();
            function pump() {
              return reader.read().then(({ done, value }) => {
                if (done) {
                  controller.close();
                  return;
                }
                controller.enqueue(value);
                return pump();
              });
            }
          },
        });
      })
      .then((stream) => new Response(stream))
      .then((response) => response.blob())
      .then((blob) => URL.createObjectURL(blob))
      .then((url) => {
        setNewContent(() => {
          return { text: paramTextToSend, isBot: true, url: url };
        });
      })
      .catch((err) => console.error(err));
    return vNewMessageObj;
  }

  useEffect(() => {
    if (dataToSpeechApi.text !== '') {
      fetchSpeechApi(dataToSpeechApi.text, voice);
    }
  }, [dataToSpeechApi, voice]);

  useEffect(() => {
    if (newContent.text !== '') {
      dispatch(updateMessagesState(newContent));
    }
  }, [newContent, dispatch]);

  // useEffect(() => {
  //   console.log(messages);
  // }, [messages]);

  return (
    <div className="card-footer">
      <form onSubmit={handleSubmit}>
        <div className="input-group">
          <span className="input-group-btn mr-2">
            <ButtonRecorder />
          </span>
          <input
            type="text"
            name="message"
            value={content.text}
            placeholder="Type Message ..."
            className="form-control rounded"
            onChange={(e) =>
              setContent(() => {
                return { text: e.target.value, isBot: false, url: '' };
              })
            }
          />
          <span className="input-group-btn ml-2">
            <button type="submit" className="btn btn-warning btn-flat">
              <i className="fas fa-paper-plane" />
            </button>
          </span>
        </div>
      </form>
    </div>
  );
};

export default Footer;
