import Layout from './Layout';
import Message from './Message';
import { useSelector } from 'react-redux';
const ChatBox = () => {
  const messages = useSelector((state) => state.messages);
  return (
    <>
      <Layout>
        {messages.messages.map((messageItem, index) => {
          if (messageItem.isBot && messageItem.url !== '') {
            return (
              <Message
                key={index}
                text={messageItem.text}
                isBot={messageItem.isBot}
                url={messageItem.url}
              />
            );
          } else if (!messageItem.isBot && messageItem.url === '') {
            return (
              <Message
                key={index}
                text={messageItem.text}
                isBot={messageItem.isBot}
                url={messageItem.url}
              />
            );
          }
          return '';
        })}
      </Layout>
    </>
  );
};

export default ChatBox;
