// eslint

import React, { useState, useEffect } from 'react';
import $ from 'jquery';
import { useDispatch } from 'react-redux';
import { updateMessagesState, fetchApi } from '../redux/messagesSlice';

const ButtonRecorder = () => {
    // const toggle = () => setRecording(!recording);
    const [recording, setRecording] = useState(false);
    const MicRecorder = require('mic-recorder-to-mp3');
    const dispatch = useDispatch();

    const [newContent, setNewContent] = useState({
        text: '',
        isBot: false,
        url: '',
    });

    // xử lý khai báo cho phép micro và các sự kiện chính
    useEffect(() => {
        // New instance
        const recorder = new MicRecorder({
            bitRate: 128,
        });

        // add event btn start
        $('#btn-start').on('click', function () {
            setRecording(true);
            recorder
                .start()
                .then(() => {
                    // console.log('start');
                })
                .catch((e) => {
                    console.log(e);
                });
        });

        // add event btn stop
        $('#btn-stop').on('click', function () {
            setRecording(false);
            if (!recording) {
                try {
                    recorder
                        .stop()
                        .getMp3()
                        .then(([buffer, blob]) => {
                            setRecording(false);
                            const file = new File(buffer, 'audio.mp3', {
                                type: blob.type,
                                lastModified: Date.now(),
                            });
                            //send mp3 file to server
                            let formData = new FormData();
                            formData.append('file', file);
                            // console.log('stop');
                            // console.log(formData);
                            $.ajax({
                                url: 'https://viettelgroup.ai/voice/api/asr/v1/rest/decode_file',
                                type: 'POST',
                                data: formData,
                                processData: !1,
                                contentType: !1,
                                success: function (response) {
                                    // console.log(response);
                                    if (Object.keys(response).length > 0) {
                                        response.map((bI) => {
                                            let vNewText = bI.result.hypotheses[0].transcript;
                                            setNewContent(() => {
                                                return { ...newContent, text: vNewText };
                                            });
                                            // console.log(newContent);
                                            return '';
                                        });
                                    }
                                },
                                error: function (e) {
                                    // console.log(e.responseText);
                                },
                            });
                        });
                } catch (error) {
                    // console.log(error);
                }
            }
        });
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        if (newContent.text !== '') {
            dispatch(updateMessagesState(newContent));
            dispatch(fetchApi(newContent));
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [newContent]);

    return (
        <>
            <button
                id="btn-start"
                className={!recording ? 'btn btn-warning' : 'btn btn-warning d-none'}
                type="button"
            >
                <i className="fas fa-microphone" />
            </button>
            <button
                id="btn-stop"
                type="button"
                className={recording ? 'btn btn-danger' : 'btn btn-danger d-none'}
            >
                <i className="fas fa-microphone-slash" />
            </button>
            {/* <audio id="audioRecord" src={url} controls />
            <a id="download">Download</a> */}
        </>
    );
};

export default ButtonRecorder;
