import React from 'react';
import Header from './Header';
import Footer from './Footer';
const Layout = ({ children }) => {
  return (
    <div className="container pb-5 d-flex justify-content-center">
      <div className="row mt-5 pt-5 justify-content-center w-100">
        <div
          className="col-12 col-sm-9 col-md-6 col-lg-4 shadow p-3 bg-white rounded"
          style={{ width: '720px' }}
        >
          <div className="card box box-warning direct-chat direct-chat-warning border mb-0">
            <Header />
            <div className="card-body">
              <div className="box-body">
                <div className="direct-chat-messages">{children}</div>
              </div>
            </div>
            <Footer />
          </div>
          <div className="row mt-2 mb-0 flex-column">
            <div className="col d-flex justify-content-center">
              <small>Bản quyền thuộc về công ty cổ phần Dinosoft</small>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Layout;
