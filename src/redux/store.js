import { configureStore } from '@reduxjs/toolkit';
import messagesReducer from './messagesSlice';
// tạo store redux
const store = configureStore({
  reducer: {
    messages: messagesReducer,
  },
});

export default store;
