import { useState, useEffect } from 'react';

const Test = () => {
  let items = [];
  const [url, setUrl] = useState('');

  const device = () => {
    navigator.mediaDevices.getUserMedia({ audio: true }).then((stream) => {
      let recorder = new MediaRecorder(stream);
      recorder.ondataavailable = (e) => {
        items.push(e.data);
        if (recorder.state == 'inactive') {
          let blob = new Blob(items, { type: 'audio/wav' });
          let url = URL.createObjectURL(blob);
          console.log(url);
          setUrl(url);
        }
      };
      recorder.start();
      setTimeout(() => {
        recorder.stop();
      }, 5000);
    });
  };

  //   const stop = () => {
  //     recorder.stop();
  //   };
  useEffect(() => {
    console.log(url);
  }, [url]);

  return (
    <div>
      <h1>this is test</h1>
      <audio src={url} controls />
      <button className="btn btn-danger" onClick={device}>
        Recorder
      </button>
      {/* <button className="btn btn-danger" onClick={stop}>
        Stop
      </button> */}
    </div>
  );
};

export default Test;
